# Coding Guidelines

## Requirements for building static sites

```bash
python3 -m venv venv
venv/bin/python3 -m pip install -r requirements.txt
```

## Running the dev server

```bash
venv/bin/mkdocs serve
```

## Building static files

```bash
venv/bin/mkdocs build
```