# Rust coding guidelines


## Rust code

- Always format your code using `rustfmt`, before committing.
- When using acronyms, keep them capitalized.
  `HTTPParser` instead of `HttpParser`.
- Promote files to module directories, when creating submodules.
- Group and sort imports into `std`, dependencies and `crate` imports.
- Define imports after module declaration.

## `Cargo.toml`

### Dependency management

It is possible in `toml` to write comments, use them to document your
dependencies.

```toml
[dependencies]
# Async runtime
tokio = { version = "^1.21" }
```

### Features

Don't activate features of your dependencies in the dependency declaration.
Use the `[features]` section instead.

```toml
[features]
default = [
    "tokio/full"
]
```

## Crate level lints

There are some lints that should be set:

- `#![warn(missing_docs)]` or even stricter
